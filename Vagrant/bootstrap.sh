#!/usr/bin/env bash
#
PROJECT_DIR="/vagrant"
COMPONENTS_DIR="${PROJECT_DIR}/Vagrant/bootstrap"
PHP_XDEBUG_INI="/etc/php/7.2/cli/conf.d/20-xdebug.ini"
PHP_PHPUNIT_PHAR_URL="https://phar.phpunit.de/phpunit-7.phar"
PHP_COMPOSER_INSTALLER_URL="https://getcomposer.org/installer"

export DEBIAN_FRONTEND=noninteractive

apt-get update -y
apt-get install -yq zip php php-mysql php-cli php-xdebug php-curl php-mbstring php-dom

cat <<EOF > /home/vagrant/.bashrc

PS1="\e[30;46m \h \e[36;43m \e[30;43m\w \e[0m\e[33;40m \e[0;0m"
EOF

echo "xdebug.remote_enable=1" >> "${PHP_XDEBUG_INI}"
echo "xdebug.remote_connect_back=1" >> "${PHP_XDEBUG_INI}"
echo "xdebug.idekey=PHP_STORM_IDEKEY" >> "${PHP_XDEBUG_INI}"

curl -sS "${PHP_COMPOSER_INSTALLER_URL}" -o /tmp/composer-setup.php
php /tmp/composer-setup.php --install-dir=/usr/local/bin --filename=composer

wget --no-verbose -O /usr/local/bin/phpunit "${PHP_PHPUNIT_PHAR_URL}"
chmod +x /usr/local/bin/phpunit
