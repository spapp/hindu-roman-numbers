<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2020
 * @license   https://opensource.org/licenses/MIT MIT License
 * @package   HinduRomanNumbers
 * @since     2020-01-22
 */

namespace Sp4pp\Test\Number;

use PHPUnit\Framework\TestCase;
use Sp4pp\Number;
use Sp4pp\Number\Integer;
use Sp4pp\Number\Roman;

class RomanTest extends TestCase {
    const TEST_VALUES = [
        1    => 'I',
        2    => 'II',
        3    => 'III',
        4    => 'IV',
        5    => 'V',
        6    => 'VI',
        7    => 'VII',
        8    => 'VIII',
        9    => 'IX',
        10   => 'X',
        15   => 'XV',
        19   => 'XIX',
        20   => 'XX',
        40   => 'XL',
        49   => 'XLIX',
        50   => 'L',
        51   => 'LI',
        90   => 'XC',
        100  => 'C',
        400  => 'CD',
        500  => 'D',
        700  => 'DCC',
        900  => 'CM',
        1000 => 'M',
        4000 => 'Mↁ',
        5000 => 'ↁ',
        8999 => 'ↁMMMCMXCIX'
    ];

    const TEST_INVALID_VALUES = ['Y', 'xi', 'XDY', true, false, []];

    public function testRomanClass () {
        $this->assertSame(true, class_exists('Sp4pp\\Number\\Roman', false));
        $this->assertSame(true, (new Roman(Roman::SIGN_1) instanceof Number));
    }

    public function testStaticFromInteger () {
        $this->assertSame(true, method_exists('Sp4pp\\Number\\Roman', 'fromInteger'));

        foreach (self::TEST_VALUES as $integer => $roman) {
            $this->assertSame($roman, Roman::fromInteger(new Integer($integer))->toString());
        }
    }

    public function testToInteger () {
        $this->assertSame(true, method_exists(new Roman(Roman::SIGN_1), 'toInteger'));

        foreach (self::TEST_VALUES as $integer => $roman) {
            $roman = new Roman($roman);

            $this->assertSame($integer, $roman->toInteger());
        }
    }

    public function testIsValid () {
        $this->methodIsValidTest(self::TEST_VALUES, true);
        $this->methodIsValidTest(self::TEST_INVALID_VALUES, false);
    }

    private function methodIsValidTest ($values, $isValid) {
        foreach ($values as $value) {
            $roman = new Roman($value);
            $this->assertSame($isValid, $roman->isValid());
        }
    }
}
