<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2020
 * @license   https://opensource.org/licenses/MIT MIT License
 * @package   HinduRomanNumbers
 * @since     2020-01-22
 */

namespace Sp4pp\Test\Number;

use PHPUnit\Framework\TestCase;
use Sp4pp\Number;
use Sp4pp\Number\Integer;

class IntegerTest extends TestCase {
    const VALID_INTEGER   = [1, 2, 3, '1', '23322332'];
    const INVALID_INTEGER = ['dsdssd', 'dsdsdsdsds.123', 'sdds.dd', '2212121.221', true, false, [], null];

    public function testIntegerClass () {
        $this->assertSame(true, class_exists('Sp4pp\\Number\\Integer'));
        $this->assertSame(true, (new Integer('1') instanceof Number));
    }

    public function testIsValid () {
        $this->methodIsValidTest(self::VALID_INTEGER, true);
        $this->methodIsValidTest(self::INVALID_INTEGER, false);
    }

    private function methodIsValidTest ($values, $isValid) {
        foreach ($values as $value) {
            $integer = new Integer($value);
            $this->assertSame($isValid, $integer->isValid());
        }
    }
}
