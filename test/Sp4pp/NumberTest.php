<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2020
 * @license   https://opensource.org/licenses/MIT MIT License
 * @package   HinduRomanNumbers
 * @since     2020-01-21
 */

namespace Sp4pp\Test;

use PHPUnit\Framework\TestCase;
use ReflectionClass;

class NumberTest extends TestCase {
    const TEST_VALUE = '123';

    private $numberMock;
    private $numberRfc;

    public function testNumberClass () {
        $this->assertSame(true, class_exists('Sp4pp\\Number'));
        $this->assertSame(true, $this->getNumberReflectionClass()->isAbstract());
    }

    public function test__construct () {
        $this->methodTest('__construct', true, true, null);
    }

    public function testGetValue () {
        $this->methodTest('getValue');

        // TODO more test
    }

    public function testToInteger () {
        $this->methodTest('toInteger', false, true, intval(self::TEST_VALUE));

        // TODO more test
    }

    public function testToString () {
        $this->methodTest('toString', false);

        // TODO more test
    }

    public function test__toString () {
        $this->methodTest('__toString');

        // TODO more test
    }

    public function testIsValid () {
        $this->methodTest('isValid', false, true, null);
    }

    private function methodTest ($methodName, $isFinal = true, $isPublic = true, $expectedValue = self::TEST_VALUE) {
        $method = $this->getNumberReflectionClass()->getMethod($methodName);

        $this->assertSame($isFinal, $method->isFinal());
        $this->assertSame($isPublic, $method->isPublic());

        if (null !== $expectedValue) {
            $this->assertSame($expectedValue, $this->getNumberMockClass()->{$methodName}());
        }
    }

    private function getNumberMockClass () {
        if (!$this->numberMock) {
            $this->numberMock = $this->getMockForAbstractClass('Sp4pp\\Number', [self::TEST_VALUE]);
        }

        return $this->numberMock;
    }

    private function getNumberReflectionClass () {
        if (!$this->numberRfc) {
            $this->numberRfc = new ReflectionClass('Sp4pp\\Number');
        }

        return $this->numberRfc;
    }
}
