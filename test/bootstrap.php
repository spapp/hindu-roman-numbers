<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2020
 * @license   https://opensource.org/licenses/MIT MIT License
 * @package   HinduRomanNumbers
 * @since     2020-01-21
 */

ini_set('display_errors', true);
ini_set('display_startup_errors', true);
error_reporting(E_ALL);

define('APPLICATION_PATH', dirname(__DIR__));

require_once(APPLICATION_PATH . '/src/Sp4pp/Number.php');
require_once(APPLICATION_PATH . '/src/Sp4pp/Number/Integer.php');
require_once(APPLICATION_PATH . '/src/Sp4pp/Number/Roman.php');
