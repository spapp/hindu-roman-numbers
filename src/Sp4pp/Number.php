<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2020
 * @license   https://opensource.org/licenses/MIT MIT License
 * @package   HinduRomanNumbers
 * @since     2020-01-21
 */

namespace Sp4pp;

abstract class Number {
    /**
     * @var mixed
     */
    protected $value;

    /**
     * Integer constructor.
     *
     * @param mixed $value
     */
    public final function __construct ($value) {
        $this->value = $value;
    }

    /**
     * Returns the original Number value
     *
     * @return mixed
     */
    public final function getValue () {
        return $this->value;
    }

    /**
     * Returns the integer value of the number
     *
     * @param int $base The base for the conversion
     *
     * @return int
     */
    public function toInteger (int $base = 10): int {
        return intval($this->getValue(), $base);
    }

    /**
     * Returns the string representation of the number
     *
     * @return int
     */
    public function toString (): string {
        return strval($this->getValue());
    }

    /**
     * Magice method
     *
     * Returns the number as a string
     *
     * @return string
     */
    public final function __toString (): string {
        return $this->toString();
    }

    /**
     * Returns TRUE if the number is valid
     *
     * @return bool
     */
    public abstract function isValid (): bool;
}
