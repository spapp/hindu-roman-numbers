<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2020
 * @license   https://opensource.org/licenses/MIT MIT License
 * @package   HinduRomanNumbers
 * @since     2020-01-22
 */

namespace Sp4pp\Number;

use RangeException;
use Sp4pp\Number;
use Sp4pp\Number\Integer;

/**
 * Class Roman
 *
 * Represents a roman number value
 *
 * @package Sp4pp
 */
class Roman extends Number {
    public const MAX_ROMAN_VALUE = 8999;

    public const SIGN_1    = 'I';
    public const SIGN_5    = 'V';
    public const SIGN_10   = 'X';
    public const SIGN_50   = 'L';
    public const SIGN_100  = 'C';
    public const SIGN_500  = 'D';
    public const SIGN_1000 = 'M';
    public const SIGN_5000 = 'ↁ';

    private const SIGNS = [
        [self::SIGN_1, self::SIGN_5],
        [self::SIGN_10, self::SIGN_50],
        [self::SIGN_100, self::SIGN_500],
        [self::SIGN_1000, self::SIGN_5000]
    ];

    private const SIGN_VALUES = [
        self::SIGN_1    => 1, self::SIGN_5 => 5,
        self::SIGN_10   => 10, self::SIGN_50 => 50,
        self::SIGN_100  => 100, self::SIGN_500 => 500,
        self::SIGN_1000 => 1000, self::SIGN_5000 => 5000
    ];

    /**
     * Returns a `Roman` value class from `Integer`
     *
     * @param \Sp4pp\Number\Integer $value
     *
     * @return Roman
     */
    public static function fromInteger (Integer $value) {
        $number      = str_split($value->toString());
        $position    = 0;
        $romanNumber = [];

        if ($value->toInteger() > self::MAX_ROMAN_VALUE) {
            throw new RangeException('Maximum supported number is ' . self::MAX_ROMAN_VALUE);
        }

        while (count($number) > 0) {
            $digit          = intval(array_pop($number));
            $signs          = self::SIGNS[$position];
            $signNextSingle = $position < count(self::SIGNS) - 1 ? self::SIGNS[$position + 1][0] : ''; // TODO
            $signSingle     = $signs[0];
            $signFifth      = $signs[1];

            if (5 === $digit) {
                array_unshift($romanNumber, $signFifth);
            } else if (4 === $digit) {
                array_unshift($romanNumber, $signSingle, $signFifth);
            } else if (9 === $digit) {
                array_unshift($romanNumber, $signSingle, $signNextSingle);
            } else {
                if ($digit < 5) {
                    array_unshift($romanNumber, join('', array_fill(0, $digit, $signSingle)));
                } else {
                    array_unshift($romanNumber, $signFifth, join('', array_fill(0, $digit - 5, $signSingle)));
                }
            }

            ++$position;
        }

        return new self(join('', $romanNumber));
    }

    /**
     * @return bool
     * @todo additional checks
     *          - The letters of the exponents of 10 (I, X, C, M) may be repeated three times in a row. Other letter
     *          may only stand alone.
     *          - If the letter I, X or C is followed by a higher valued letter,
     *          then neither the bigger nor the smaller valued letter may be repeated. e.g.: correct: XCIX and not
     *          correct: XXCX, XCC, XCXX
     *          - Letter I may only be followed by V or X. Letter X may only be followed by L or C.
     *          Letter C may only be followed by D or M. e.g.: correct: IV and not correct: IM
     *          - Handle SIGN_5000
     *
     * @inheritDoc
     */
    public function isValid (): bool {
        $pattern = join('', [
            '~^[',
            self::SIGN_1, self::SIGN_5,
            self::SIGN_10, self::SIGN_50,
            self::SIGN_100, self::SIGN_500,
            self::SIGN_1000, self::SIGN_5000,
            ']+$~'
        ]);

        return is_string($this->getValue()) && preg_match($pattern, $this->toString());
    }

    /**
     * @inheritDoc
     *
     * @param int $base
     *
     * @return int
     */
    public function toInteger (int $base = 10): int {
        $number   = preg_split('//u', $this->toString(), -1, PREG_SPLIT_NO_EMPTY);
        $preSigns = [self::SIGN_1, self::SIGN_10, self::SIGN_100, self::SIGN_1000];
        $intValue = 0;

        while (count($number) > 0) {
            $digit     = array_shift($number);
            $value     = self::SIGN_VALUES[$digit];
            $valueNext = count($number) > 0 ? self::SIGN_VALUES[$number[0]] : 0;

            if ($value === $valueNext or $value > $valueNext) {
                $intValue += $value;
            } else if (in_array($digit, $preSigns) and $value < $valueNext) {
                $intValue += ($valueNext - $value);
                array_shift($number);
            }
        }

        return intval($intValue, $base);
    }
}
