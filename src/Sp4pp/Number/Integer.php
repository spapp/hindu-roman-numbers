<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2020
 * @license   https://opensource.org/licenses/MIT MIT License
 * @package   HinduRomanNumbers
 * @since     2020-01-22
 */

namespace Sp4pp\Number;

use Sp4pp\Number;

/**
 * Class Integer
 *
 * Represents a integer number value
 *
 * @package Sp4pp
 */
class Integer extends Number {
    /**
     * @inheritDoc
     * @return bool
     */
    public function isValid (): bool {
        return is_numeric($this->getValue()) && preg_match('~^[0-9]+$~', $this->toString());
    }
}
