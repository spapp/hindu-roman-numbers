# Roman numeric system

In the Roman numeric system, Roman numbers are denoted by letters. Different letters hold different values as you can see here:

- 1 = I
- 5 = V
- 10 = X
- 50 = L
- 100 = C
- 500 = D
- 1000 = M

- [Wiki - Roman numerals](https://hu.wikipedia.org/wiki/Római_szám%C3%ADrás)
- [Wiki - Hindu-arabic numerals](https://hu.wikipedia.org/wiki/Hindu–arab_szám%C3%ADrás)

## Getting Started

### `Prerequisites`
- Vagrant
- Virtual Box
- PHP 7.2.24

### `Installing`

#### `Installing dev environment`

1. git clone https://bitbucket.org/spapp/hindu-roman-numbers.git
2. cd /path/to/hindu-roman-numbers
3. vagrant up
4. vagrant ssh
5. cd /vagrant

## Running the tests

Run `phpunit` command in `/vagrant` folder.

## Convert Hindu-arabic numerals to Roman numerals

Run `int_to_roman` command in `/vagrant/bin` folder.
If run without parameter then will show the manual.

## License

**MIT License**

Copyright 2020 spappsite.hu

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
