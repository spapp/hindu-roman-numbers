#!/usr/bin/env bash
#
# NAME
#     gen_ints.sh -- generates integers
#
# SYNOPSIS
#     gen_ints.sh [-h|--help] [count [outputfile]]
#
# DESCRIPTION
#     This script generates integers and write it into the output file.
#
# ARGUMENTS
#   -h      print manual
#
#   Positional arguments:
#     count        - Generated integer numbers count.
#     outputfile   - Simple text file. Each line will contain a integer number.
#                    If the file is exists then it will be overwriten
#

case ${1} in
    '-h' | '--help') sed -e "1,2d;19q" "${0}" | sed -e "s/# \?//";exit 0;;
esac

COUNT=${1:-1000}
FILE=${2:-int.txt}

truncate -s 0 ${FILE}

for i in $(seq 1 ${COUNT}); do
    echo $(shuf -i 1-3999 -n 1) >> ${FILE};
done
