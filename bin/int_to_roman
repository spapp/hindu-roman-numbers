#!/usr/bin/env php
<?php
/**
 * NAME
 *      int_to_random -- converts Arabic-Hindu numbers to Roman numbers
 *
 * SYNOPSIS
 *      int_to_random inputfile outputfile
 *
 * DESCRIPTION
 *      This script reads Arabic-Hindu numbers between 0 and 4000 from the input file,
 *      converts them to Roman numbers and writes the resulting "numbers" into the output file.
 *
 *      If you do not have `inputfile` then you can use `gen_ints.sh` script.
 *
 *      Positional arguments:
 *          inputfile   - Simple text file. Each line contains a Arabic-Hindu number.
 *          outputfile  - Simple text file. Each line will contain a Roman number.
 */

ini_set('display_errors', true);
ini_set('display_startup_errors', true);
error_reporting(E_ALL);

if ('cli' !== substr(php_sapi_name(), 0, 3)) {
    throw new Exception('Use it cli.');
}

define('APPLICATION_PATH', dirname(__DIR__));

require_once(APPLICATION_PATH . '/src/Sp4pp/Number.php');
require_once(APPLICATION_PATH . '/src/Sp4pp/Number/Integer.php');
require_once(APPLICATION_PATH . '/src/Sp4pp/Number/Roman.php');

use Sp4pp\Number\Integer;
use Sp4pp\Number\Roman;

if (3 !== $argc) {
    echo str_replace(['*/', '*'], '', join('', array_slice(file(__FILE__), 3, 16)));
    exit(1);
}

$inputFile  = $argv[1];
$outputFile = $argv[2];
$lines      = 0;

try {
    if (file_exists($outputFile)) {
        throw new Exception('The output file already exists');
    }

    $in = fopen($inputFile, 'r');

    if (is_resource($in)) {
        $out = fopen($outputFile, 'w');

        echo 'Conversion is in progress...' . PHP_EOL;

        while (!feof($in)) {
            $line = trim(fgets($in));

            if ($line) {
                $number = new Integer($line);

                if ($number->toInteger() < 0 or $number->toInteger() > 4000) {
                    throw new Exception('Out of range (0-4000): ' . $number);
                } else {
                    $roman = Roman::fromInteger($number);

                    fwrite($out, $roman . PHP_EOL);
                    ++$lines;
                }
            }
        }
    } else {
        throw new Exception('Input file can not open');
    }

    fclose($in);
    fclose($out);
} catch (Exception $error) {
    echo $error . PHP_EOL;
    exit(1);
}

echo sprintf(
    "Conversion is done.\nProcessed\t%s lines\nInput file\t%s\nOutput file\t%s\n",
    number_format($lines, 0, '.', ' '),
    $inputFile,
    $outputFile
);
